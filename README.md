# Stuart Frontend Challenge

Link to the repo: https://bitbucket.org/serenafal/stuart-frontend-challenge/src/main/

### Setup

`git clone` https://bitbucket.org/serenafal/stuart-frontend-challenge

From the project root run `yarn` to install dependencies.

`yarn dev` - run the app in dev mode. Go to http://localhost:8080 to view it in the browser.

`yarn build` - build the app to the `dist` folder.

### Branches

So the way I approached this was to first do the base challenge but by manually setting up webpack and babel. After that was done I focused on the remaning bonus points.
Once I completed the challenge I thought I'd have a go at the GraphQL APIs option, mostly for fun as I don't have a lot of experience with it and wanted to give it a try.

So on the [`main`](https://bitbucket.org/serenafal/stuart-frontend-challenge/src/main/) branch you can find the complete challenge with all the bonus points:

- setup without any frameworks
- use Google Maps API without any libraries
- geocode addresses when user stops typing
- disable submit button while request is pending
- automatically dismiss toast notification

Other branches:

[`develop`](https://bitbucket.org/serenafal/stuart-frontend-challenge/src/develop/) - base challenge + manual setup without framekworks. I've used [`google-map-react`](https://github.com/google-map-react/google-map-react#readme) as Google Maps library

[`feature/bonus-points`](https://bitbucket.org/serenafal/stuart-frontend-challenge/src/7b5a962a9d4c93fb08fc1a1610823d98932c159d/?at=feature%2Fbonus-points) - challenge bonus points

[`feature/graphql-api`](https://bitbucket.org/serenafal/stuart-frontend-challenge/src/1a0657023507ae8ce197e636566c3dbe3d877a49/?at=feature%2Fgraphql-api) - WIP of switching from REST APIs to GraphQL

### Time spent

It took me about 4h30m~ to do the basic challenge with the manual webpack/babel setup. And then about 1h~ for the remaining bonus points, the longest being implementing Maps APIs without any libraries.

### Improvements

There are a few things I would improve or add if I had more time:

- If this were an actual product I would split webpack config into development and production, in order to have lighter and minified code and assets optimization.
- I think it would make sense to wait for the map to be loaded and ready before showing the job creation form to the user. Having some sort of loading indication would be nice.
- The map's default coordinates on load are Paris coordinates. Ideally I would maybe request permission to the user's location in order to show them an area that would be relevant to them.
- As of right now geocoding happens on blur and when the user stops typing. I think it would be nice to also geocode the address when the user selects it from the browser's suggestion list, as in that case there is no typing and geocoding only happens on blur.
- I would like to make it so that the map moves to the center point between the selected locations once you've typed in two valid addresses. I already added this in the base version of the challenge where I've used `google-map-react`, and in that case it would probably make sense to reset the map position and zoom once the job has been created.
- It would be nice to have a small animation for the toast notification, even just a slide-down/slide-up which would be more interesting than just appearing and disappearing.
- I've encountered a bug with the map on Firefox that would cause the position and zoom to be wrong (I found myself in the middle of the Pacific Ocean 🙃), but I can't seem to reproduce it consistently so I haven't had the chance to understand what caused it yet.
- In the version where I've used the Google Maps APIs without any libraries, I'm not particularly happy with how I handled markers creation and deletion, especially the latter. I'm using an `id` to identify which marker is pick up location and which is drop off location, and then using that id to delete them. I think this is ok in this case where we only have two markers that will always have the same purpose, but I don't think it's a great solution. I think ideally I would want to use the position information on the markers to be able to differentiate them and be sure I would be deleteting the marker at the given position.
- If the API requests fail for whatever reason (e.g. network error), there is no indication to the user. I guess I would add a generic message like "Something went wrong. Please try again".
- I would like to continue (and improve) the work on the `feature/graphql-api` branch.
