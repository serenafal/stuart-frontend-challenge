import config from "@config/api";

export const apiRequest = async (method, endpoint, body, headers) => {
  const options = {
    method: method,
    headers: { "Content-Type": "application/json", ...headers },
    body: body ? JSON.stringify(body) : null,
  };

  const baseUrl = config.apiBaseUrl;

  const result = await fetch(`${baseUrl}/${endpoint}`, options);

  if (process.env.NODE_ENV === "development") {
    console.log({ method, endpoint, options, result });
  }

  if (!result.ok) {
    const error = await result.json();
    throw error;
  } else {
    return await result.json();
  }
};
