import { useState, useEffect, useRef } from "react";

export default (initialState) => {
  const [isToastVisible, setIsToastVisible] = useState(initialState);
  const ref = useRef(null);

  const handleOutsideClick = (event) => {
    // dimiss toast if user clicked anywhere outside of it
    if (ref.current && !ref.current.contains(event.target)) {
      setIsToastVisible(false);
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleOutsideClick, true);
    return () => {
      document.removeEventListener("click", handleOutsideClick, true);
    };
  });

  return { ref, isToastVisible, setIsToastVisible };
};
