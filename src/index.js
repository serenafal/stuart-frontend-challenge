import React from "react";
import ReactDOM from "react-dom";

import MapView from "@components/MapView";

import "@styles/fonts.css";
import "@styles/normalize.css";
import "@styles/theme.css";
import "@styles/typography.css";

const App = () => {
  return <MapView />;
};

ReactDOM.render(<App />, document.getElementById("root"));
