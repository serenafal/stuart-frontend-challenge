import React from "react";
import "./style.css";

const Toast = (props) => {
  const { message } = props;

  return <div className="toast">{message}</div>;
};

export default Toast;
