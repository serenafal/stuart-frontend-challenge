import React, { useEffect, useRef, useCallback } from "react";
import "./style.css";

import config from "@config/api";

import dropOffMarker from "@assets/svg/dropOffMarker.svg";
import pickUpMarker from "@assets/svg/pickUpMarker.svg";

const Map = (props) => {
  const { dropOffCoords, pickUpCoords } = props;

  let mapRef = useRef(null);

  let markers = useRef([]);

  const defaultProps = {
    center: {
      lat: 48.8698578,
      lng: 2.3345886,
    },
    zoom: 15,
  };

  const onScriptReady = () => {
    mapRef = new window.google.maps.Map(
      document.getElementById("map"),
      defaultProps
    );
  };

  const addPickUpMarker = useCallback((coords) => {
    const marker = new google.maps.Marker({
      position: coords,
      title: "Pick up location",
      map: mapRef,
      icon: pickUpMarker,
      id: "pickup",
    });

    markers.current.push(marker);
  }, []);

  const addDropOffMarker = useCallback((coords) => {
    const marker = new google.maps.Marker({
      position: coords,
      title: "Drop off location",
      map: mapRef,
      icon: dropOffMarker,
      id: "dropoff",
    });
    markers.current.push(marker);
  }, []);

  const removeMarker = (id) => {
    markers.current.forEach((marker) => {
      if (marker.id === id) {
        marker.setMap(null);
      }
    });
  };

  useEffect(() => {
    if (pickUpCoords) {
      addPickUpMarker(pickUpCoords);
    } else {
      removeMarker("pickup");
    }
  }, [pickUpCoords]);

  useEffect(() => {
    if (dropOffCoords) {
      addDropOffMarker(dropOffCoords);
    } else {
      removeMarker("dropoff");
    }
  }, [dropOffCoords]);

  useEffect(() => {
    // async script loading
    if (!window.google) {
      const mapsScript = document.createElement("script");
      mapsScript.type = "text/javascript";
      mapsScript.src = `https://maps.google.com/maps/api/js?key=${config.mapsApiKey}`;
      const scriptNode = document.getElementsByTagName("script")[0];
      scriptNode.parentNode.insertBefore(mapsScript, scriptNode);
      mapsScript.addEventListener("load", (e) => {
        onScriptReady();
      });
    } else {
      onScriptReady();
    }
  }, []);

  return <div style={{ width: "100%", height: "100%" }} id="map" />;
};

export default Map;
