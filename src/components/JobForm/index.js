import React from "react";
import "./style.css";

import pickUpBadgeBlank from "@assets/svg/pickUpBadgeBlank.svg";
import pickUpBadgePresent from "@assets/svg/pickUpBadgePresent.svg";
import pickUpBadgeError from "@assets/svg/pickUpBadgeError.svg";
import dropOffBadgeBlank from "@assets/svg/dropOffBadgeBlank.svg";
import dropOffBadgePresent from "@assets/svg/dropOffBadgePresent.svg";
import dropOffBadgeError from "@assets/svg/dropOffBadgeError.svg";

const JobForm = (props) => {
  const {
    onChange,
    onBlur,
    onSubmit,
    pickUpAddress,
    dropOffAddress,
    isLoading,
    submitEnabled,
  } = props;

  return (
    <form className="job-form" onSubmit={onSubmit}>
      <div className="job-form__input-wrapper">
        <img
          src={
            pickUpAddress?.error
              ? pickUpBadgeError
              : pickUpAddress?.coords
              ? pickUpBadgePresent
              : pickUpBadgeBlank
          }
          alt="Pick up location"
        />
        <input
          id="pickUp"
          type="text"
          value={pickUpAddress?.value}
          placeholder="Pick up address"
          onChange={onChange}
          onBlur={onBlur}
        />
      </div>
      <div className="job-form__input-wrapper">
        <img
          src={
            dropOffAddress?.error
              ? dropOffBadgeError
              : dropOffAddress?.coords
              ? dropOffBadgePresent
              : dropOffBadgeBlank
          }
          alt="Drop off location"
        />
        <input
          id="dropOff"
          type="text"
          value={dropOffAddress?.value}
          placeholder="Drop off address"
          onChange={onChange}
          onBlur={onBlur}
        />
      </div>
      <input
        className="job-form__submit-button"
        disabled={!submitEnabled || isLoading}
        type="submit"
        value={isLoading ? "Creating..." : "Create job"}
      />
    </form>
  );
};

export default JobForm;
