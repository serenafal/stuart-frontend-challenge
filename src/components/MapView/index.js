import React, { useCallback, useState, useEffect, useRef } from "react";
import "./style.css";

import Map from "@components/Map";
import JobForm from "@components/JobForm";
import Toast from "@components/Toast";
import { apiRequest } from "@api";
import useToast from "@hooks/useToast";

const MapView = () => {
  const initialState = {
    value: "",
    coords: null,
    error: null,
  };

  const [pickUpAddress, setPickUpAddress] = useState(initialState);
  const [dropOffAddress, setDropOffAddress] = useState(initialState);

  const [isLoading, setIsLoading] = useState(false);

  const { ref, isToastVisible, setIsToastVisible } = useToast(false);

  // use refs to be able to clear timeout on blur in order to avoid a double api request
  const pickUpAddressTimer = useRef(null);
  const dropOffAddressTimer = useRef(null);

  // geocode addresses when user stops typing
  useEffect(() => {
    pickUpAddressTimer.current = setTimeout(() => {
      if (pickUpAddress.value) {
        geocodeAddress("pickUp");
      }
    }, 800);

    return () => clearTimeout(pickUpAddressTimer.current);
  }, [pickUpAddress.value]);

  useEffect(() => {
    dropOffAddressTimer.current = setTimeout(() => {
      if (dropOffAddress.value) {
        geocodeAddress("dropOff");
      }
    }, 800);

    return () => clearTimeout(dropOffAddressTimer.current);
  }, [dropOffAddress.value]);

  // make toast notification disappear automatically after 5s
  useEffect(() => {
    let toastTimer;

    if (isToastVisible) {
      toastTimer = setTimeout(() => {
        setIsToastVisible(false);
      }, 5000);
    }

    if (toastTimer) {
      return () => clearTimeout(toastTimer);
    }
  }, [isToastVisible]);

  const geocodeAddress = async (addressType) => {
    const body =
      addressType === "pickUp"
        ? { address: pickUpAddress.value }
        : { address: dropOffAddress.value };

    try {
      const response = await apiRequest("POST", "geocode", body);

      const { latitude, longitude } = response;

      if (addressType === "pickUp") {
        setPickUpAddress({
          ...pickUpAddress,
          coords: { lat: latitude, lng: longitude },
          error: false,
        });
      } else {
        setDropOffAddress({
          ...dropOffAddress,
          coords: { lat: latitude, lng: longitude },
          error: false,
        });
      }
    } catch (error) {
      console.log("Error while geocoding address: ", error);

      if (error.code === "GEOCODE_ERROR") {
        if (addressType === "pickUp") {
          setPickUpAddress({ ...pickUpAddress, error: true });
        } else {
          setDropOffAddress({ ...dropOffAddress, error: true });
        }

        return;
      }
    }
  };

  const onChange = useCallback((e) => {
    const target = e.currentTarget;
    if (target.id === "pickUp") {
      setPickUpAddress({ value: target.value });
    } else {
      setDropOffAddress({ value: target.value });
    }
  }, []);

  const onBlur = (e) => {
    const target = e.currentTarget;

    if (pickUpAddressTimer.current) {
      clearTimeout(pickUpAddressTimer.current);
    }

    if (dropOffAddressTimer.current) {
      clearTimeout(dropOffAddressTimer.current);
    }

    if (target.value === "") {
      return;
    }

    geocodeAddress(target.id);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const body = { pickup: pickUpAddress.value, dropoff: dropOffAddress.value };

    try {
      await apiRequest("POST", "jobs", body);
      setIsLoading(false);
      // show toast notification
      setIsToastVisible(true);
      // reset form
      setPickUpAddress(initialState);
      setDropOffAddress(initialState);
    } catch (error) {
      setIsLoading(false);
      console.log("Error while attempting to create job: ", error);
    }
  };

  return (
    <>
      <div className="map-view">
        {isToastVisible && (
          <div className="map-view__toast-notification" ref={ref}>
            <Toast message="Job has been created successfully!" />
          </div>
        )}
        <div className="map-view__form">
          <JobForm
            onChange={onChange}
            onBlur={onBlur}
            onSubmit={onSubmit}
            pickUpAddress={pickUpAddress}
            dropOffAddress={dropOffAddress}
            isLoading={isLoading}
            submitEnabled={pickUpAddress.coords && dropOffAddress.coords}
          />
        </div>
        <div className="map-view__map">
          <Map
            pickUpCoords={pickUpAddress.coords}
            dropOffCoords={dropOffAddress.coords}
          />
        </div>
      </div>
    </>
  );
};

export default MapView;
