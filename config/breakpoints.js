const tablet = 768;
const laptop = 1024;
const wide = 1440;

module.exports = {
  tablet,
  laptop,
  wide,
  customMedia: {
    "--tablet": `(min-width: ${tablet}px)`,
    "--laptop": `(min-width: ${laptop}px)`,
    "--wide": `(min-width: ${wide}px)`,
  },
};
