module.exports = () => {
  const plugins = {
    autoprefixer: {},
    "postcss-nested": {},
    "postcss-custom-media": {
      importFrom: "config/breakpoints.js",
    },
    "postcss-preset-env": {
      stage: 4,
    },
  };

  return {
    plugins,
  };
};
