const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const projectRoot = path.resolve(__dirname, "..");
const dist = path.join(projectRoot, "dist");
const src = path.join(projectRoot, "src");
const babelrc = path.join(projectRoot, "config/.babelrc");
const postCssConfig = path.join(projectRoot, "config/postcss.config.js");

module.exports = {
  entry: path.join(src, "index.js"),
  output: {
    path: dist,
    filename: "[name].js",
  },
  mode: process.env.NODE_ENV || "development",
  resolve: {
    modules: [path.resolve(src), "node_modules"],
    alias: {
      "@assets": path.resolve(src, "assets"),
      "@components": path.resolve(src, "components"),
      "@styles": path.resolve(src, "styles"),
      "@config": path.resolve(projectRoot, "config"),
      "@api": path.resolve(src, "api"),
      "@hooks": path.resolve(src, "hooks"),
    },
  },
  devServer: {
    static: src,
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            extends: babelrc,
            cacheDirectory: true,
          },
        },
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          { loader: "css-loader", options: { importLoaders: 1 } },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                config: postCssConfig,
              },
            },
          },
        ],
      },
      {
        test: /\.(jpg|jpeg|png|gif|svg)$/,
        use: ["file-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(src, "index.html"),
    }),
  ],
};
